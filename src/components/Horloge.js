import React, { Component } from 'react'
import moment from 'moment'
import "moment/locale/fr"
import { useEffect, useState } from 'react'

moment().locale("fr")
function Jours(props) {
    let today = moment(props.date).format("dddd")
    return (
        <div>
            <h1 className="text-uppercase">{today}</h1>
        </div>
    )
}
function Heure(props) {
    return (
        <div>
            <h1>{moment(props.date).format("HH:mm")}</h1>
        </div>
    )
}

function LaDate(props) {
    return (
        <div>
            <h3 >{moment(props.date).format("LL")}</h3>
        </div>
    )
}

class Timing extends Component {
    render() {

        let date = moment(this.props.date)
        if (date.hour() >= 0 && date.hour() <= 11) {
            return (<h3 className="text-uppercase">Matin</h3>)

        } else if (date.hour() >= 12 && date.hour() <= 17) {
            return (<h3 className="text-uppercase">Après midi</h3>)
        } else {
            return (<h3 className="text-uppercase">Soir</h3>)
        }
    }
}


function Horloge(props) {
    const [date, setdate] = useState(new Date());
    useEffect(() => {
        setInterval(() => {
            setdate(new Date())
        }, 1000)
    }, []);
    return (
        <div className="container d-flex justify-content-center my-5">
           <div class="card" style={{ width: "400px", backgroundColor: "#eeee", }}>
                <div class="card " style={{ width: "300px", margin: "50px 50px 50px 50px", backgroundColor: "black" }}>
                    <div class="card-body text-white text-center">
                        <Jours date={date} />
                        <Timing date={date} />
                        <Heure date={date} />
                        <LaDate date={date} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Horloge
